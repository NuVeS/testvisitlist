//
//  ViewController.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Presenter;

@protocol ViewControllerInputProtocol <NSObject>

-(void)colorMarkWithInfo:(NSDictionary*)info;
-(void)reloadTable;

@end

@interface ViewController : UIViewController<ViewControllerInputProtocol>

@property (nonatomic, strong) Presenter* presenter;
@property (nonatomic, strong) NSArray<NSDictionary*>* annotations;

@end

