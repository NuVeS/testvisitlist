//
//  ServiceController.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LoadService;
@class ParseService;

typedef void  (^ServeCompletionBlock)(NSArray<NSDictionary*>*);

@interface ServiceController : NSObject

-(void)loadVisitsListWithCompletion: (ServeCompletionBlock)completion failure: (void(^)(NSError*))failure;

-(void)loadOrganizationListWithCompletion: (ServeCompletionBlock)completion failure: (void(^)(NSError*))failure;

@end
