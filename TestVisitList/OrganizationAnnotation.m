//
//  OrganizationAnnotation.m
//  TestVisitList
//
//  Created by Максуд on 14.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "OrganizationAnnotation.h"


@implementation OrganizationAnnotation



- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    self = [super init];
    if (self) {
        self.coordinate = coordinate;
    }
    return self;
}

@end
