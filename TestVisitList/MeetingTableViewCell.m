//
//  MeetingTableViewCell.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "MeetingTableViewCell.h"

@interface MeetingTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *visitName;

@property (weak, nonatomic) IBOutlet UILabel *organizationName;

@end

@implementation MeetingTableViewCell

-(void)setVisit:(NSString *)visit{
    self.visitName.text = visit;
}

-(void)setOrganization:(NSString *)organization{
    self.organizationName.text = organization;
}

@end
