//
//  MeetingTableViewCell.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeetingTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *visit;
@property (nonatomic, strong) NSString *organization;

@end
