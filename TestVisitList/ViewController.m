//
//  ViewController.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "ViewController.h"
#import "Presenter.h"
#import <MapKit/MapKit.h>
#import "OrganizationAnnotation.h"
#import "Constants.h"

@interface ViewController ()<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mainTableVIew;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) NSMutableArray<MKAnnotationView*> *annotationViews;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.presenter = [[Presenter alloc] initWithView:self];
    [self.presenter loadData];
    
    self.mainTableVIew.delegate = self.presenter.tableBuilder;
    self.mainTableVIew.dataSource = self.presenter.tableBuilder;
    
}

-(void)setAnnotations:(NSArray<NSDictionary *> *)annotations{
    self.annotationViews = @[].mutableCopy;
    [self.mapView removeAnnotations:self.mapView.annotations];
    for(NSDictionary* dict in annotations){
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake([dict[LatitudeKey] doubleValue], [dict[LongitudeKey] doubleValue]);
        OrganizationAnnotation *annotation = [[OrganizationAnnotation alloc] init];
        [annotation setCoordinate:location];
        annotation.information = @{OrganizationIdKey:dict[OrganizationIdKey]};
        
        [self.mapView addAnnotation:annotation];
    }
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
    if([self.presenter.activeVisit[OrganizationIdKey] isEqualToString:((OrganizationAnnotation*)annotation).information[OrganizationIdKey]]){
        annView.pinTintColor = [UIColor greenColor];
    }else{
        annView.pinTintColor = [UIColor redColor];
    }
    [self.annotationViews addObject:annView];
    return annView;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    [self.presenter didSelectMarkOnMap:((OrganizationAnnotation*)view.annotation).information];
}

-(void)colorMarkWithInfo:(NSDictionary*)info{
    for(MKPinAnnotationView* pin in self.annotationViews){
        if([((OrganizationAnnotation*)pin.annotation).information[OrganizationIdKey] isEqualToString:info[OrganizationIdKey]]){
            pin.pinTintColor = [UIColor greenColor];
            continue;
        }
        pin.pinTintColor = [UIColor redColor];
    }
}

-(void)reloadTable{
    [self.mainTableVIew reloadData];
}

@end
