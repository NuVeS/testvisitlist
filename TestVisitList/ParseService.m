//
//  ParseService.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "ParseService.h"

@implementation ParseService

-(void)parseData:(NSData *)data completion:(ParseCompletionBlock)completion error:(void (^)(NSError *))errorBlock{
    @autoreleasepool {
        NSError *error = nil;
        id object = [NSJSONSerialization
                     JSONObjectWithData:data
                     options:0
                     error:&error];
        
        if(error || ![object isKindOfClass:[NSArray class]]) {
            errorBlock(error);
            return;
        }else{
            NSArray *results = object;
            completion(results);
        }
    }
}

@end
