//
//  URLs.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef URLs_h
#define URLs_h

extern NSString *const VisitsListURL;
extern NSString *const OrganizationListURL;

#endif /* URLs_h */
