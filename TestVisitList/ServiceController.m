//
//  ServiceController.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "ServiceController.h"
#import "LoadService.h"
#import "ParseService.h"
#import "URLs.h"

@interface ServiceController()
@property (nonatomic, strong) LoadService* loadService;
@property (nonatomic, strong) ParseService* parseService;
@end

@implementation ServiceController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.loadService = [[LoadService alloc] init];
        self.parseService = [[ParseService alloc] init];
    }
    return self;
}

-(void)loadVisitsListWithCompletion:(ServeCompletionBlock)completion failure:(void (^)(NSError *))failure{
    __weak typeof(self) weakSelf = self;
    [self.loadService getDataWithURL:VisitsListURL completion:^(NSData *data){
        [weakSelf didLoadData:data withCompletion:completion failure:failure];
    }];
}

-(void)loadOrganizationListWithCompletion:(ServeCompletionBlock)completion failure:(void (^)(NSError *))failure{
    __weak typeof(self) weakSelf = self;
    [self.loadService getDataWithURL:OrganizationListURL completion:^(NSData *data){
        [weakSelf didLoadData:data withCompletion:completion failure:failure];
    }];
}

-(void)didLoadData:(NSData*)data withCompletion:(void(^)(NSArray<NSDictionary*>*))completion failure:(void(^)(NSError*))failure{
    [self.parseService parseData:data completion:^(NSArray<NSDictionary*>* results) {
        completion(results);
    } error:failure];
}

@end
