//
//  ParseService.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void  (^ParseCompletionBlock)(NSArray<NSDictionary*>*);

@interface ParseService : NSObject

-(void)parseData:(NSData*)data completion:(ParseCompletionBlock)completion error:(void(^)(NSError*))error;

@end
