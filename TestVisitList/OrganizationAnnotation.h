//
//  OrganizationAnnotation.h
//  TestVisitList
//
//  Created by Максуд on 14.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface OrganizationAnnotation : NSObject<MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSDictionary* information;

-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
