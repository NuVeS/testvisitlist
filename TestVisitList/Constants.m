//
//  Constants.m
//  TestVisitList
//
//  Created by Максуд on 14.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "Constants.h"

NSString *const TitleKey = @"title";
NSString *const OrganizationNameKey = @"organization";
NSString *const OrganizationIdKey = @"organizationId";
NSString *const LatitudeKey = @"latitude";
NSString *const LongitudeKey = @"longitude";

