//
//  Constants.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "URLs.h"

NSString *const VisitsListURL = @"http://demo3526062.mockable.io/getVisitsListTest";
NSString *const OrganizationListURL = @"http://demo3526062.mockable.io/getOrganizationListTest";
