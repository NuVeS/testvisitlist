//
//  LoadService.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void  (^LoadCompletionBlock)(NSData*);

@interface LoadService : NSObject

-(void)getDataWithURL:(NSString*)urlString completion:(LoadCompletionBlock)completion;

@end
