//
//  TableBuilder.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "TableBuilder.h"
#import "MeetingTableViewCell.h"
#import "Presenter.h"
#import "Constants.h"

@implementation TableBuilder

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.visits.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.visit = self.visits[indexPath.row][TitleKey];
    cell.organization = self.visits[indexPath.row][OrganizationNameKey];
    
    if([self.visits[indexPath.row][OrganizationIdKey] isEqualToString:self.presenter.activeVisit[OrganizationIdKey]] ){
        cell.contentView.backgroundColor = [UIColor greenColor];
    }else{
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSDictionary* info = @{OrganizationIdKey:self.visits[indexPath.row][OrganizationIdKey]};
    [self.presenter didSelectCellWithInfo:info];
}

@end
