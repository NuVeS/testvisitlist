//
//  TableBuilder.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Presenter;

@interface TableBuilder : NSObject<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) Presenter* presenter;

@property (nonatomic, strong) NSArray<NSDictionary*>* visits;

@end
