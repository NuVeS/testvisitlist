//
//  Constants.h
//  TestVisitList
//
//  Created by Максуд on 14.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef Constants_h
#define Constants_h

extern NSString *const TitleKey;
extern NSString *const OrganizationNameKey;
extern NSString *const OrganizationIdKey;
extern NSString *const LatitudeKey;
extern NSString *const LongitudeKey;

#endif /* Constants_h */
