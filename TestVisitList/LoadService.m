//
//  LoadService.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "LoadService.h"

@implementation LoadService

-(void)getDataWithURL:(NSString *)urlString completion:(LoadCompletionBlock)completion{
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              completion(data.copy);
                                          }];
    
    [downloadTask resume];
}

@end
