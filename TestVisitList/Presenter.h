//
//  Presenter.h
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableBuilder.h"
#import "ServiceController.h"
#import "ViewController.h"

@protocol PresenterInputProtocol <NSObject>

-(void)didSelectCellWithInfo:(NSDictionary*)info;
-(void)didSelectMarkOnMap:(NSDictionary*)info;
-(void)loadData;

@end

@interface Presenter : NSObject<PresenterInputProtocol>

@property (nonatomic, strong) TableBuilder* tableBuilder;
@property (nonatomic, strong) ServiceController* serviceControler;
@property (nonatomic, weak) ViewController* view;

@property (nonatomic, strong) NSArray<NSDictionary*>* visits;
@property (nonatomic, strong) NSArray<NSDictionary*>* organizations;
@property (nonatomic, strong) NSDictionary* activeVisit;

-(instancetype)initWithView:(ViewController*)view;

@end
