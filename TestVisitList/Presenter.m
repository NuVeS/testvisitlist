//
//  Presenter.m
//  TestVisitList
//
//  Created by Максуд on 13.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "Presenter.h"
#import "Constants.h"


@interface Presenter()

@end

@implementation Presenter

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableBuilder = [[TableBuilder alloc] init];
        self.serviceControler = [[ServiceController alloc] init];
        
        self.tableBuilder.presenter = self;
    }
    return self;
}

-(instancetype)initWithView:(ViewController*)view{
    self = [self init];
    if (self){
        self.view = view;
    }
    return self;
}

-(void)loadData{
    __weak typeof(self) weakSelf = self;
    [self.serviceControler loadVisitsListWithCompletion:^(NSArray<NSDictionary *> *result) {
        weakSelf.visits = result;
        [weakSelf.serviceControler loadOrganizationListWithCompletion:^(NSArray<NSDictionary *> *result) {
            weakSelf.organizations = result;
            NSArray<NSDictionary*>* dict = [weakSelf prepareDictForShow];
            weakSelf.tableBuilder.visits = dict;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.view reloadTable];
                [weakSelf.view setAnnotations:dict];
            });
        } failure:nil];
    } failure:nil];
}

-(NSArray<NSDictionary*>*)prepareDictForShow{
    NSMutableArray<NSDictionary*>* array = @[].mutableCopy;
    
    for(NSDictionary* visit in self.visits){
        for(NSDictionary *organizaiton in self.organizations){
            if([visit[OrganizationIdKey] isEqualToString:organizaiton[OrganizationIdKey]]){
                NSDictionary *newVisit = @{TitleKey:visit[TitleKey],OrganizationNameKey:organizaiton[TitleKey],
                            LatitudeKey:organizaiton[LatitudeKey], LongitudeKey:organizaiton[LongitudeKey],
                                           OrganizationIdKey:visit[OrganizationIdKey]};
                [array addObject:newVisit];
                break;
            }
        }
    }
    
    return array;
}

-(void)didSelectMarkOnMap:(NSDictionary*)info{
    self.activeVisit = info;
    [self.view reloadTable];
    [self.view colorMarkWithInfo:info];
}

-(void)didSelectCellWithInfo:(NSDictionary*)info{
    self.activeVisit = info;
    [self.view reloadTable];
    [self.view colorMarkWithInfo:info];
}

@end
